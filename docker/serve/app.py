import io
import numpy as np
import cv2
import model
from utils import load_yaml_conf
import torch
import torchvision.transforms as transforms
from PIL import Image
from flask import Flask, jsonify, request


app = Flask(__name__)


IMAGE_SIZES = {'model_a': 512,
               'model_b': 50,
               'model_c': 128}
DEVICE = "cpu"
CLASSES = {
    'model_a': {
        0: "Negative",
        1: "Positive"
    },
    'model_b': {
        0: 'Green - 1',
        1: 'Green - 2',
        2: 'Green - 3',
        3: 'Green - 4',
        4: 'Green - 5',
        5: 'Not grass'
    },
    'model_c': {
        0: 'Not grass',
        1: 'Other color scale - 1',
        2: 'Other color scale - 2',
        3: 'Other color scale - 3',
        4: 'Other color scale - 4',
        5: 'Other color scale - 5'
    }
}

OUT_FEATURES = {
    'model_a': 2,
    'model_b': 6,
    'model_c': 6
}

MEAN_STD = {
    'model_a': {
        'mean': [0.4710553288459778, 0.5687215328216553, 0.29849615693092346],
        'std': [0.1597992181777954, 0.14489658176898956, 0.15662230551242828]
    },
    'model_b': {
        'mean': [0.5930973291397095, 0.6398319005966187, 0.4285174310207367],
        'std': [0.1774233877658844, 0.1463237702846527, 0.22104938328266144]
    },
    'model_c': {
        'mean': [0.3878757357597351, 0.45674413442611694, 0.26277956366539],
        'std': [0.1673697978258133, 0.11587931960821152, 0.14419788122177124]
    }
}


def model_preparation():
    model_conf = load_yaml_conf(path="model.yml")
    model_a = model.Net(conf=model_conf,
                        data_h_w=IMAGE_SIZES['model_a'],
                        out_features=OUT_FEATURES['model_a'],
                        device=DEVICE)
    best_checkpoint_path = 'model/model_a.pth'
    checkpoint = torch.load(best_checkpoint_path)
    model_a.load_state_dict(checkpoint['model_state_dict'])
    model_b = model.Net(conf=model_conf,
                        data_h_w=IMAGE_SIZES['model_b'],
                        out_features=OUT_FEATURES['model_b'],
                        device=DEVICE)
    best_checkpoint_path = 'model/model_b.pth'
    checkpoint = torch.load(best_checkpoint_path)
    model_b.load_state_dict(checkpoint[0])
    model_c = model.Net(conf=model_conf,
                        data_h_w=IMAGE_SIZES['model_c'],
                        out_features=OUT_FEATURES['model_c'],
                        device=DEVICE)
    best_checkpoint_path = 'model/model_c.pth'
    checkpoint = torch.load(best_checkpoint_path)
    model_c.load_state_dict(checkpoint[0])
    return {'model_a': model_a, 'model_b': model_b, 'model_c': model_c}


def transform_image(image_bytes, model):
    transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((IMAGE_SIZES[model], IMAGE_SIZES[model])),
        transforms.ToTensor(),
        transforms.Normalize(
            **MEAN_STD[model]
        )
    ])
    image = cv2.cvtColor(np.array(Image.open(io.BytesIO(image_bytes))), cv2.COLOR_BGR2RGB)
    return transform(image).unsqueeze(0).to(DEVICE)


def get_prediction(image_bytes, model):
    tensor = transform_image(image_bytes=image_bytes, model=model)
    outputs = MODELS[model](tensor)
    outputs = outputs.detach().numpy()
    predicted_idx = np.argmax(outputs[0])
    return CLASSES[model][predicted_idx], outputs[0][predicted_idx]


MODELS = model_preparation()


@app.route('/model_a/predict', methods=['POST'])
def predict_a():
    if request.method == 'POST':
        _file = request.files['file']
        img_bytes = _file.read()
        class_name, probability = get_prediction(image_bytes=img_bytes, model='model_a')
        return jsonify({'class_name': class_name, 'probability': float(probability)})


@app.route('/model_b/predict', methods=['POST'])
def predict_b():
    if request.method == 'POST':
        _file = request.files['file']
        img_bytes = _file.read()
        class_name, probability = get_prediction(image_bytes=img_bytes, model='model_b')
        return jsonify({'class_name': class_name, 'probability': float(probability)})


@app.route('/model_c/predict', methods=['POST'])
def predict_c():
    if request.method == 'POST':
        _file = request.files['file']
        img_bytes = _file.read()
        class_name, probability = get_prediction(image_bytes=img_bytes, model='model_c')
        return jsonify({'class_name': class_name, 'probability': float(probability)})


if __name__ == "__main__":
    app.run(host='0.0.0.0')
