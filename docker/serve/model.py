import torch.nn as nn
import collections
import pathlib
import torch
import utils


class Net(nn.Module):
    def __init__(self, conf, data_h_w: int, out_features: int, device: str = 'cpu'):
        super().__init__()
        self._device = device
        self.seq_modules = nn.Sequential(self._prepare_nn_conf(conf=conf, data_h_w=data_h_w, out_features=out_features))
        self.to(self._device)

    def forward(self, x):
        logits = self.seq_modules(x)
        return nn.Softmax(dim=1)(logits)

    @staticmethod
    def _prepare_nn_conf(conf, data_h_w, out_features):
        order_dict = collections.OrderedDict()
        for _seq in conf["model"]["seq"]:
            order_dict.update(**_seq)
        h, w = None, None
        for _key in conf['model']['compute_in_features']['from_layers']:
            if h is None and w is None:
                h, w = utils.conv2d_output_shape(h_w=data_h_w, kernel_size=order_dict[_key]['kernel_size'],
                                                 stride=order_dict[_key].get('stride', 1), pad=0, dilation=1)
                continue
            h, w = utils.conv2d_output_shape(h_w=(h, w), kernel_size=order_dict[_key]['kernel_size'],
                                             stride=order_dict[_key].get('stride', 1), pad=0, dilation=1)
        out_channels = order_dict[conf['model']['compute_in_features']['use_in_channels_from']]['out_channels']
        order_dict[conf['model']['compute_in_features']['update_in_features']]['in_features'] = out_channels * h * w
        order_dict[conf['model']['compute_in_features']['update_out_features']]['out_features'] = out_features
        for _key in order_dict:
            _tmp_m = None
            if 'conv' in _key:
                _tmp_m = nn.Conv2d(**order_dict[_key])
            elif 'relu' in _key:
                _tmp_m = nn.ReLU()
            elif 'max_pool2d' in _key:
                _tmp_m = nn.MaxPool2d(**order_dict[_key])
            elif 'linear' in _key:
                _tmp_m = nn.Linear(**order_dict[_key])
            elif 'flatten' in _key:
                _tmp_m = nn.Flatten(**order_dict[_key])
            order_dict[_key] = _tmp_m
        return order_dict

    def save(self, epochs, optimizer, criterion, path):
        """
        Function to save the trained model to disk.
        """
        pathlib.Path(path).mkdir(parents=True, exist_ok=True)
        torch.save({
                    'epoch': epochs,
                    'model_state_dict': self.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    'loss': criterion,
                    }, f"{path}/model.pth")
