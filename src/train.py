import os
from functools import partial

import ray
import torch
import torch.nn as nn
import torch.optim as optim
import optuna
from ray import tune
from ray.air import session, Checkpoint, RunConfig, CheckpointConfig
from ray.tune import CLIReporter
from ray.tune.schedulers import AsyncHyperBandScheduler
from ray.tune.search import ConcurrencyLimiter
from ray.tune.search.optuna import OptunaSearch

import datasets
import model
from utils import load_yaml_conf

runtime_env = {"working_dir": ".", "pip": ["matplotlib", "tqdm"]}


def train(config, data_dir=None, model_conf=None, epochs=10):
    dataset = datasets.Dataset(train_test_split_factor=0.1, image_resize=128, batch_size=int(config["batch_size"]))
    dataset_train, dataset_valid, dataset_classes = dataset.get_datasets(root_dir=data_dir)
    train_loader, valid_loader = dataset.get_data_loaders(dataset_train=dataset_train, dataset_valid=dataset_valid)
    device = ('cuda' if torch.cuda.is_available() else 'cpu')
    net = model.Net(conf=model_conf,
                    data_h_w=128,
                    out_features=len(dataset_classes),
                    device=device)

    if device == 'cuda' and torch.cuda.device_count() > 1:
        net = nn.DataParallel(net)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), lr=config["lr"], momentum=config["momentum"])
    loaded_checkpoint = session.get_checkpoint()
    if loaded_checkpoint:
        with loaded_checkpoint.as_directory() as loaded_checkpoint_dir:
            model_state, optimizer_state = torch.load(os.path.join(loaded_checkpoint_dir, "checkpoint.pt"))
        net.load_state_dict(model_state)
        optimizer.load_state_dict(optimizer_state)

    for epoch in range(1, epochs + 1):  # loop over the dataset multiple times
        running_loss = 0.0
        epoch_steps = 0
        for i, data in enumerate(train_loader, 0):
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            epoch_steps += 1
            if i % 2000 == 1999:  # print every 2000 mini-batches
                print("[%d, %5d] loss: %.3f" % (epoch + 1, i + 1,
                                                running_loss / epoch_steps))
                running_loss = 0.0

        # Validation loss
        val_loss = 0.0
        val_steps = 0
        total = 0
        correct = 0
        for i, data in enumerate(valid_loader, 0):
            with torch.no_grad():
                inputs, labels = data
                inputs, labels = inputs.to(device), labels.to(device)

                outputs = net(inputs)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()

                loss = criterion(outputs, labels)
                val_loss += loss.cpu().numpy()
                val_steps += 1

        # with tune.checkpoint_dir(epoch) as checkpoint_dir:
        #     path = os.path.join(checkpoint_dir, "checkpoint")
        #     torch.save((net.state_dict(), optimizer.state_dict()), path)
        os.makedirs("model_tmp", exist_ok=True)
        torch.save(
            (net.state_dict(), optimizer.state_dict()), "model_tmp/checkpoint.pt")
        checkpoint = Checkpoint.from_directory("model_tmp")
        session.report(dict(loss=(val_loss / val_steps), accuracy=correct / total, training_iteration=epoch),
                       checkpoint=checkpoint)
    print("Finished Training")


def main(num_samples=10, max_num_epochs=10, gpus_per_trial=2):
    ray.init(address='ray://localhost:10001', runtime_env=runtime_env)
    model_conf = load_yaml_conf(path="../resources/model.yml")
    data_dir = os.path.abspath("/input")
    algo = OptunaSearch(
        space={
            "momentum":  optuna.distributions.FloatDistribution(0.1, 0.9),
            "lr": optuna.distributions.FloatDistribution(1e-4, 1e-1, log=True),
            "batch_size": optuna.distributions.CategoricalDistribution([2, 4, 8, 16])
        },
        metric='loss',
        mode='min'
    )
    algo = ConcurrencyLimiter(algo, max_concurrent=1)
    scheduler = AsyncHyperBandScheduler(
        max_t=max_num_epochs,
        grace_period=1,
        reduction_factor=2
    )
    trainable_with_cpu_gpu = tune.with_resources(partial(train,
                                                         data_dir=data_dir,
                                                         model_conf=model_conf,
                                                         epochs=max_num_epochs),
                                                 {"cpu": 4, "gpu": 1})
    reporter = CLIReporter(metric_columns=["loss", "accuracy", "training_iteration"])
    tuner = tune.Tuner(
        trainable_with_cpu_gpu,
        run_config=RunConfig(local_dir="/shared/storage/",
                             progress_reporter=reporter,
                             sync_config=tune.SyncConfig(),
                             checkpoint_config=CheckpointConfig(
                                 checkpoint_score_attribute="min-loss",
                                 num_to_keep=5,
                             )
        ),
        tune_config=tune.TuneConfig(
            search_alg=algo,
            scheduler=scheduler,
            num_samples=num_samples,
            metric="loss",
            mode="min"
        )
    )
    results = tuner.fit()

    print("Best hyperparameters found were: ", results.get_best_result().config)

    best_result = results.get_best_result(metric="loss", mode="min", scope="all")
    print("Best result config: {}".format(best_result.config))
    print("Best result final validation loss: {}".format(
        best_result.metrics["loss"]))
    print("Best result final validation accuracy: {}".format(
        best_result.metrics["accuracy"]))
    checkpoint_dir = '../output/shared/{}/model_tmp/'.format(
        '/'.join(best_result.log_dir.resolve().as_posix().split('/')[3:])
    )
    print("Best checkpoint dir: {}".format(checkpoint_dir))


if __name__ == "__main__":
    # You can change the number of GPUs per trial here:
    main(num_samples=100, max_num_epochs=100, gpus_per_trial=1)
