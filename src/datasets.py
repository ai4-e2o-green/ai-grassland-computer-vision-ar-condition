from filelock import FileLock
import torch
import torchvision
import torch.utils.data as tud


class Dataset(object):
    def __init__(self,
                 train_test_split_factor: float = 0.1,
                 image_resize: int = 512,
                 batch_size: int = 8,
                 num_workers: int = 4) -> None:
        self._train_test_split_factor = train_test_split_factor
        self._image_resize = image_resize
        self._batch_size = batch_size
        self._num_workers = num_workers

    @staticmethod
    def get_mean_and_std(dataloader):
        channels_sum, channels_squared_sum, num_batches = 0, 0, 0
        for data, _ in dataloader:
            # Mean over batch, height and width, but not over the channels
            channels_sum += torch.mean(data, dim=[0, 2, 3])
            channels_squared_sum += torch.mean(data ** 2, dim=[0, 2, 3])
            num_batches += 1
        mean = channels_sum / num_batches
        #  std = sqrt(E[X^2] - (E[X])^2)
        std = (channels_squared_sum / num_batches - mean ** 2) ** 0.5
        return mean.tolist(), std.tolist()

    def get_train_transform(self, mean, std):
        return torchvision.transforms.Compose([
            torchvision.transforms.Resize(size=(self._image_resize, self._image_resize)),
            torchvision.transforms.RandomHorizontalFlip(p=0.5),
            torchvision.transforms.RandomVerticalFlip(p=0.5),
            #torchvision.transforms.GaussianBlur(kernel_size=(5, 9), sigma=(0.1, 2.0)),
            #torchvision.transforms.RandomAdjustSharpness(sharpness_factor=2, p=0.5),
            #torchvision.transforms.RandomAutocontrast(p=0.5),
            #torchvision.transforms.RandomGrayscale(p=0.5),
            torchvision.transforms.RandomRotation(degrees=45),
            torchvision.transforms.RandomPerspective(distortion_scale=0.6, p=0.5),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize(
                mean=mean,
                std=std
            )
        ])

    def get_valid_transform(self, mean, std):
        return torchvision.transforms.Compose([
            torchvision.transforms.Resize((self._image_resize, self._image_resize)),
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Normalize(
                mean=mean,
                std=std
            )
        ])

    def get_datasets(self, root_dir: str):
        """
        Function to prepare the Datasets.
        Returns the training and validation datasets along
        with the class names.
        """
        dataset = torchvision.datasets.ImageFolder(
            root=root_dir,
            transform=torchvision.transforms.Compose([
                torchvision.transforms.Resize(size=(self._image_resize, self._image_resize)),
                torchvision.transforms.ToTensor()
            ])
        )
        loader = torch.utils.data.DataLoader(
            dataset=dataset, batch_size=self._batch_size,
            num_workers=self._num_workers, shuffle=True
        )
        mean, std = self.get_mean_and_std(dataloader=loader)
        print(mean, std)
        dataset = torchvision.datasets.ImageFolder(
            root=root_dir,
            transform=(self.get_train_transform(mean=mean, std=std))
        )
        dataset_test = torchvision.datasets.ImageFolder(
            root=root_dir,
            transform=(self.get_valid_transform(mean=mean, std=std))
        )
        dataset_size = len(dataset)
        # Calculate the validation dataset size.
        valid_size = int(self._train_test_split_factor*dataset_size)
        # Radomize the data indices.
        indices = torch.randperm(len(dataset)).tolist()
        # Training and validation sets.
        dataset_train = tud.Subset(dataset=dataset, indices=indices[:-valid_size])
        dataset_valid = tud.Subset(dataset=dataset_test, indices=indices[-valid_size:])
        return dataset_train, dataset_valid, dataset.classes

    def get_data_loaders(self, dataset_train, dataset_valid):
        """
        Prepares the training and validation data loaders.
        :param dataset_train: The training dataset.
        :param dataset_valid: The validation dataset.
        Returns the training and validation data loaders.
        """
        with FileLock(".data.lock"):
            train_loader = tud.DataLoader(
                dataset=dataset_train, batch_size=self._batch_size,
                shuffle=True, num_workers=self._num_workers
            )
            valid_loader = tud.DataLoader(
                dataset_valid, batch_size=self._batch_size,
                shuffle=False, num_workers=self._num_workers
            )
        return train_loader, valid_loader
