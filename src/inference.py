import torch
import cv2
import numpy as np
import glob as glob
import os
from torchvision import transforms
import model
from utils import load_yaml_conf
# Constants.
DATA_PATH = '../input/Negative'
IMAGE_SIZE = 256
DEVICE = 'cpu'
# Class names.
class_names = ['Positive',
               'Negative']

conf = load_yaml_conf(path='../resources/model.yml')
# Load the trained model.
model = model.Net(
        conf=conf,
        data_h_w=IMAGE_SIZE,
        out_features=len(class_names),
        device=DEVICE
    )
checkpoint = torch.load('../output/model.pth', map_location=DEVICE)
print('Loading trained model weights...')
model.load_state_dict(checkpoint['model_state_dict'])

# Get all the test image paths.
all_image_paths = glob.glob(f"{DATA_PATH}/*")
# Iterate over all the images and do forward pass.
for image_path in all_image_paths:
    # Get the ground truth class name from the image path.
    gt_class_name = image_path.split(os.path.sep)[-2].split('.')[0]
    # Read the image and create a copy.
    print(image_path)
    image = cv2.imread(image_path)
    orig_image = image.copy()

    # Preprocess the image
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((IMAGE_SIZE, IMAGE_SIZE)),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=[0.4710553288459778, 0.5687215328216553, 0.29849615693092346],
            std=[0.1597992181777954, 0.14489658176898956, 0.15662230551242828]
        )
    ])
    image = transform(image)
    image = torch.unsqueeze(image, 0)
    image = image.to(DEVICE)

    # Forward pass throught the image.
    outputs = model(image)
    outputs = outputs.detach().numpy()
    pred_class_name = class_names[np.argmax(outputs[0])]
    print(f"GT: {gt_class_name}, Pred: {pred_class_name.lower()}")
    # Annotate the image with ground truth.
    cv2.putText(
        orig_image, f"GT: {gt_class_name}",
        (10, 25), cv2.FONT_HERSHEY_SIMPLEX,
        0.8, (0, 255, 0), 2, lineType=cv2.LINE_AA
    )
    # Annotate the image with prediction.
    cv2.putText(
        orig_image, f"Pred: {pred_class_name.lower()}",
        (10, 55), cv2.FONT_HERSHEY_SIMPLEX,
        0.8, (100, 100, 225), 2, lineType=cv2.LINE_AA
    )
    cv2.imwrite(f"../output/{gt_class_name}.png", orig_image)
