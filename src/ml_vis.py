import io
import numpy as np
import cv2
from src.model import Net
from src.utils import load_yaml_conf
import torch
import torchvision.transforms as transforms
from PIL import Image
import matplotlib.pyplot as plt

from torchviz import make_dot


IMAGE_SIZES = {'model_a': 512,
               'model_b': 50,
               'model_c': 128}
DEVICE = "cpu"
CLASSES = {
    'model_a': {
        0: "Negative",
        1: "Positive"
    },
    'model_b': {
        0: 'Green - 1',
        1: 'Green - 2',
        2: 'Green - 3',
        3: 'Green - 4',
        4: 'Green - 5',
        5: 'Not grass'
    },
    'model_c': {
        0: 'Not grass',
        1: 'Other color scale - 1',
        2: 'Other color scale - 2',
        3: 'Other color scale - 3',
        4: 'Other color scale - 4',
        5: 'Other color scale - 5'
    }
}

OUT_FEATURES = {
    'model_a': 2,
    'model_b': 6,
    'model_c': 6
}

MEAN_STD = {
    'model_a': {
        'mean': [0.4710553288459778, 0.5687215328216553, 0.29849615693092346],
        'std': [0.1597992181777954, 0.14489658176898956, 0.15662230551242828]
    },
    'model_b': {
        'mean': [0.5930973291397095, 0.6398319005966187, 0.4285174310207367],
        'std': [0.1774233877658844, 0.1463237702846527, 0.22104938328266144]
    },
    'model_c': {
        'mean': [0.3878757357597351, 0.45674413442611694, 0.26277956366539],
        'std': [0.1673697978258133, 0.11587931960821152, 0.14419788122177124]
    }
}


def model_preparation():
    model_conf = load_yaml_conf(path="../resources/model.yml")
    model_a = Net(conf=model_conf,
                  data_h_w=IMAGE_SIZES['model_a'],
                  out_features=OUT_FEATURES['model_a'],
                  device=DEVICE)
    best_checkpoint_path = '../docker/serve/model/model_a.pth'
    checkpoint = torch.load(best_checkpoint_path)
    model_a.load_state_dict(checkpoint['model_state_dict'])
    model_b = Net(conf=model_conf,
                  data_h_w=IMAGE_SIZES['model_b'],
                  out_features=OUT_FEATURES['model_b'],
                  device=DEVICE)
    best_checkpoint_path = '../docker/serve/model/model_b.pth'
    checkpoint = torch.load(best_checkpoint_path)
    model_b.load_state_dict(checkpoint[0])
    model_c = Net(conf=model_conf,
                  data_h_w=IMAGE_SIZES['model_c'],
                  out_features=OUT_FEATURES['model_c'],
                  device=DEVICE)
    best_checkpoint_path = '../docker/serve/model/model_c.pth'
    checkpoint = torch.load(best_checkpoint_path)
    model_c.load_state_dict(checkpoint[0])
    return {'model_a': model_a, 'model_b': model_b, 'model_c': model_c}


def transform_image(image_bytes, model):
    transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.Resize((IMAGE_SIZES[model], IMAGE_SIZES[model])),
        transforms.ToTensor(),
        transforms.Normalize(
            **MEAN_STD[model]
        )
    ])
    image = cv2.cvtColor(np.array(image_bytes), cv2.COLOR_BGR2RGB)
    return transform(image).unsqueeze(0).to(DEVICE)


MODELS = model_preparation()


def main():
    image_path = '../input/Not grass/100_0019_0344b.JPG'
    image = cv2.imread(image_path)

    # Preprocess the image
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    for _model in MODELS:
        transformed_image = transform_image(image_bytes=image, model=_model)
        y = MODELS[_model](transformed_image)
        make_dot(y.mean(),
                 params=dict(MODELS[_model].named_parameters()),
                 show_attrs=True,
                 show_saved=True).render(filename='../output/{}_vis'.format(_model), format='png')


if __name__ == '__main__':
    main()
