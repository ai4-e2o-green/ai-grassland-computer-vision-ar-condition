# e2o-torch-ml

Machine learning model lib for image classification with pytorch.

## Model architecture

![Architecture overview](docs/images/ar_models.drawio.png "Architecture overview")

## Neural networks

### Model A

Input image shape: `512x512`

Number of classes: 2

![Model A NN Vis](docs/images/model_a_vis.png "Model A NN Vis")

### Model B

Input image shape: `50x50`

Number of classes: 6

![Model B NN Vis](docs/images/model_b_vis.png "Model B NN Vis")

### Model C

Input image shape: `128x128`

Number of classes: 6

![Model C NN Vis](docs/images/model_c_vis.png "Model C NN Vis")
